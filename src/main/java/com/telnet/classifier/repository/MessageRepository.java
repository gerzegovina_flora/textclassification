package com.telnet.classifier.repository;

import com.telnet.classifier.data.Message;
import org.bson.Document;

import java.util.List;

public interface MessageRepository {

    List<Document> getNotIndexedMessages();

    void markMessagesAsIndexed(List<Message> messages);

}
