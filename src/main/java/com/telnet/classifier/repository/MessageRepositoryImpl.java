package com.telnet.classifier.repository;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.telnet.classifier.data.Message;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.mongodb.client.model.Filters.eq;

@Repository
public class MessageRepositoryImpl implements MessageRepository {

    MongoClient mongoClient = new MongoClient("localhost");

    @Override
    public List<Document> getNotIndexedMessages() {
        MongoCollection<Document> collection = mongoClient.getDatabase("acme")
                .getCollection("post");
        FindIterable<Document> cursor = collection.find(eq("indexed", false));
        return StreamSupport.stream(cursor.spliterator(), false)
                .collect(Collectors.toList());
    }

    @Override
    public void markMessagesAsIndexed(List<Message> messages) {
        messages.forEach(message -> {
            Bson filter = eq("_id", new ObjectId(message.getId()));
            BasicDBObject updateFields = new BasicDBObject();
            updateFields.append("indexed", true);
            updateFields.append("topicClass", message.getTopicClass().toString());
            BasicDBObject setQuery = new BasicDBObject();
            setQuery.append("$set", updateFields);
            mongoClient.getDatabase("acme").getCollection("post")
                    .updateOne(filter, setQuery);
        });
    }
}
