package com.telnet.classifier.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.telnet.classifier.data.Message;
import com.telnet.classifier.data.MessageIndexDocument;
import com.telnet.classifier.data.TopicClass;
import com.telnet.classifier.exception.ClassifyTextException;
import com.telnet.classifier.repository.MessageRepository;
import com.telnet.classifier.util.MessageFormatter;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.env.Environment;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@EnableAsync
@PropertySource(value = "classpath:topic-channels.properties")
public class ClassifierServiceImpl implements ClassifierService {

    private final MessageRepository messageRepository;
    private final ConversionService conversionService;
    private final Environment environment;

    ClientConfiguration clientConfiguration =
            ClientConfiguration.builder().connectedTo("localhost:9200").build();
    RestHighLevelClient client = RestClients.create(clientConfiguration).rest();

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public ClassifierServiceImpl(MessageRepository messageRepository, ConversionService conversionService, Environment environment) {
        this.messageRepository = messageRepository;
        this.conversionService = conversionService;
        this.environment = environment;
    }

    @Async
    @Scheduled(fixedRate = 5000)
    @Override
    public void classifyMessages() {
        List<Message> messages = messageRepository.getNotIndexedMessages().stream()
                .map(this::convertDocumentToMessage)
                .peek(this::classifyMessage)
                .collect(Collectors.toList());
        updateMessagesInMongo(messages);
        try {
            // indexNewMessages(messages);
        } catch (RuntimeException e) {
            // ADD LOGGER
        }
    }

    private void updateMessagesInMongo(List<Message> messages) {
        messageRepository.markMessagesAsIndexed(messages);
    }

    private void indexNewMessages(List<Message> messages) throws IOException {
        List<MessageIndexDocument> formattedList = messages.stream()
                .map(message -> {
                    MessageIndexDocument messageIndexDocument = new MessageIndexDocument();
                    messageIndexDocument.setTopic(message.getTopicClass());
                    messageIndexDocument.setMessageText(message.getText());
                    return messageIndexDocument;
                })
                .collect(Collectors.toList());

        for (MessageIndexDocument messageIndexDocument : formattedList) {
            String jsonObject = OBJECT_MAPPER.writeValueAsString(messageIndexDocument);
            IndexRequest request = new IndexRequest("message");
            request.source(jsonObject, XContentType.JSON);
            client.index(request, RequestOptions.DEFAULT);
        }
    }

    private Message classifyMessage(Message message) {
        TopicClass topicClass = getTopicClass(message);
        message.setTopicClass(topicClass);
        return message;
    }

    private TopicClass getTopicClass(Message message) {
        if (isMessageNotNeededToClassify(message)) {
            return getTopicByChannelTheme(message);
        }

        String textToClassify = MessageFormatter.formatMessage(message.getText());
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder()
                .query(QueryBuilders.matchQuery("messageText", textToClassify));

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.searchType(SearchType.DFS_QUERY_THEN_FETCH);
        searchRequest.source(searchSourceBuilder);

        try {
            SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
            SearchHit[] searchHits = searchResponse.getHits().getHits();
            if (searchHits.length > 0) {
                String topic = searchHits[0].getSourceAsMap().get("topic").toString();
                return TopicClass.valueOf(topic);
            }
            return TopicClass.OTHER;
        } catch (IOException e) {
            throw new ClassifyTextException();
        }
    }

    private boolean isMessageNotNeededToClassify(Message message) {
        String channelTopic = environment.getProperty(message.getChannelId());
        return StringUtils.isNotEmpty(channelTopic) && EnumUtils.isValidEnum(TopicClass.class, channelTopic);
    }

    private TopicClass getTopicByChannelTheme(Message message) {
        String channelTopic = environment.getProperty(message.getChannelId());
        return TopicClass.valueOf(channelTopic);
    }

    private Message convertDocumentToMessage(Document document) {
        return conversionService.convert(document, Message.class);
    }

}
