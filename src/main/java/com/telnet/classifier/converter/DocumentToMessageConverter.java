package com.telnet.classifier.converter;


import com.telnet.classifier.data.Message;
import org.bson.Document;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class DocumentToMessageConverter implements Converter<Document, Message> {

    @Override
    public Message convert(Document document) {
        Message message = new Message();
        message.setText(document.getString("textMessage"));
        message.setId(document.get("_id").toString());
        message.setChannelId(String.valueOf(document.getLong("channelId")));
        return message;
    }
}
