package com.telnet.classifier.data;

public class MessageIndexDocument {

    private TopicClass topic;
    private String messageText;

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public TopicClass getTopic() {
        return topic;
    }

    public void setTopic(TopicClass topic) {
        this.topic = topic;
    }
}
