package com.telnet.classifier.data;

public class Message {

    private String id;
    private String channelId;
    private String text;
    private TopicClass topicClass;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public TopicClass getTopicClass() {
        return topicClass;
    }

    public void setTopicClass(TopicClass topicClass) {
        this.topicClass = topicClass;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }
}
