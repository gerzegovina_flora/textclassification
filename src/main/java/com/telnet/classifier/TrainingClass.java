package com.telnet.classifier;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;
import com.telnet.classifier.data.MessageIndexDocument;
import com.telnet.classifier.data.TopicClass;
import com.telnet.classifier.util.MessageFormatter;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;

import java.io.IOException;
import java.io.Reader;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class TrainingClass {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static void main(String[] args) throws IOException, URISyntaxException, CsvException {
        Reader reader = Files.newBufferedReader(Paths.get(
                ClassLoader.getSystemResource("messages.csv").toURI()));

        CSVReader csvReader = new CSVReaderBuilder(reader)
                .withCSVParser(new CSVParserBuilder()
                        .withSeparator(';')
                        .build()).build();

        List<String[]> list = csvReader.readAll();
        reader.close();
        csvReader.close();

        // TODO: remove hash tags! RegEx
        List<MessageIndexDocument> formattedList = list.stream()
                .peek(textToText -> textToText[1] = MessageFormatter.formatMessage(textToText[1]))
                .map(textToText -> {
                    MessageIndexDocument messageIndexDocument = new MessageIndexDocument();
                    messageIndexDocument.setTopic(TopicClass.valueOf(textToText[0]));
                    messageIndexDocument.setMessageText(textToText[1]);
                    return messageIndexDocument;
                })
                .collect(Collectors.toList());

        ClientConfiguration clientConfiguration =
                ClientConfiguration.builder().connectedTo("localhost:9200").build();
        RestHighLevelClient client = RestClients.create(clientConfiguration).rest();

        DeleteIndexRequest request2 = new DeleteIndexRequest("message");

        client.indices().delete(request2, RequestOptions.DEFAULT);

        for (MessageIndexDocument messageIndexDocument : formattedList) {
            String jsonObject = OBJECT_MAPPER.writeValueAsString(messageIndexDocument);
            IndexRequest request = new IndexRequest("message");
            request.source(jsonObject, XContentType.JSON);

            IndexResponse response = client.index(request, RequestOptions.DEFAULT);
            String index = response.getIndex();
            long version = response.getVersion();
        }
    }

}
