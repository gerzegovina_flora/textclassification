package com.telnet.classifier;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Collections;

@SpringBootApplication
@EnableScheduling
public class TelNetClassifierStarter {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(TelNetClassifierStarter.class);
        app.setDefaultProperties(Collections
                .singletonMap("server.port", "8092"));
        app.run(args);
    }

}
