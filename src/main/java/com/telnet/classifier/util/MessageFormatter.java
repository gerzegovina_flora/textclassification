package com.telnet.classifier.util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class MessageFormatter {

    private static List<String> STOP_WORDS;

    static {
        try {
            try (Stream<String> lines = Files.lines(Paths.get(
                    ClassLoader.getSystemResource("stop-words.txt").toURI()))) {
                STOP_WORDS = lines.collect(Collectors.toList());
            }
        } catch (IOException | URISyntaxException e) {
            STOP_WORDS = Collections.emptyList();
        }
    }

    private MessageFormatter() {
    }

    public static String formatMessage(String message) {
        String messageToProcess = message.toLowerCase();
        messageToProcess = removeSymbols(messageToProcess);
        return removeReluctantWords(messageToProcess);
    }

    private static String removeSymbols(String text) {
        return text.replaceAll("[-+.^:,!#«»%()/0-9@№?—\\-\n]", " ");
    }

    private static String removeReluctantWords(String text) {
        return Arrays.stream(text.split(" "))
                .map(word -> STOP_WORDS.contains(word) ? "" : word)
                .collect(Collectors.joining(" "));
    }

}
